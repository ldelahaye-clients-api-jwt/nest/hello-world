FROM node:10-alpine

LABEL maintainer="delahaye.ludovic1@gmail.com"

# Create workdir
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
RUN mkdir -p /etc/ma/
# Copy configuration file 
ENV APPLICATION_MAHELLO_ENV /etc/ma/mahello.docker.env
WORKDIR /home/node/app

# COPY Source code
COPY --chown=node:node docker/mahello.env.docker /etc/ma/mahello.docker.env

RUN ls -la

# expose port 8080
EXPOSE 8080
EXPOSE 3000