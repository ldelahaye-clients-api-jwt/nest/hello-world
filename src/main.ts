import * as fs from 'fs';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('MA Hello')
    .setDescription('This is a template for new API in Micro Application Architecture based on JWT security')
    .setVersion('1.0')
    .addTag('worlds')
    .addTag('admin-worlds')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'access-token',
    )
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-docs', app, document);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);

  fs.readFile('ASCII_ART.txt', 'utf8', function (err, data) {
    if (err) throw err;
    console.log(data);
  });
}
bootstrap();
