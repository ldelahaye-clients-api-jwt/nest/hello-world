import { Injectable, BadRequestException } from '@nestjs/common';
import World from '../world/world.entity';
import { WorldRepository } from '../world/world.repository';
import { MyLogger } from 'src/logger/logger';
import { WorldUpdateDto } from '../world/dto/world.update.dto';
import { WorldPatchDto } from '../world/dto/world.patch.dto';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';
import { SelectQueryBuilder } from 'typeorm';

@Injectable()
export class AdminWorldService {
    constructor(private worldRepository: WorldRepository, private LOGGER: MyLogger) {
        this.LOGGER.setContext("AdminWorldService");
    }

    async find(id: string): Promise<World> {
        this.LOGGER.debug('find', id);
        return await this.worldRepository.findOneOrFail(id);
    }

    async findAll(userId: string, options: IPaginationOptions): Promise<Pagination<World>> {
        const queryBuilder: SelectQueryBuilder<World> = this.worldRepository.createQueryBuilder('w');
        if (userId) {
            queryBuilder.where('w.userId= :userId', { userId });
        }
        queryBuilder.addOrderBy('w.updated', 'DESC');
        this.LOGGER.debug(JSON.stringify(options));
        return await paginate<World>(queryBuilder, options);
    }

    async update(idWorld: string, worldUpdateDto: WorldUpdateDto): Promise<World> {
        this.LOGGER.debug("update", idWorld);
        const world: World = await this.worldRepository.findOneOrFail(idWorld);
        const { id, name } = worldUpdateDto;
        if (id != idWorld) {
            throw new BadRequestException('ids.not.match')
        }
        world.name = name;
        return await this.worldRepository.save(world);
    }

    async patch(idWorld: string, worldPatchDto: WorldPatchDto): Promise<World> {
        this.LOGGER.debug("update", idWorld);
        const { name } = worldPatchDto;

        const world: World = await this.worldRepository.findOneOrFail(idWorld);
        //Patch only name attribut
        world.name = name;

        return await this.worldRepository.save(world);
    }

    async delete(idWorld: string): Promise<void> {
        this.LOGGER.debug('delete', idWorld);
        await this.worldRepository.softDelete(idWorld);
    }
}
