import { Test, TestingModule } from '@nestjs/testing';
import { AdminWorldController } from './admin-world.controller';

describe('AdminWorld Controller', () => {
  let controller: AdminWorldController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdminWorldController],
    }).compile();

    controller = module.get<AdminWorldController>(AdminWorldController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
