import { Test, TestingModule } from '@nestjs/testing';
import { AdminWorldService } from './admin-world.service';

describe('AdminWorldService', () => {
  let service: AdminWorldService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdminWorldService],
    }).compile();

    service = module.get<AdminWorldService>(AdminWorldService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
