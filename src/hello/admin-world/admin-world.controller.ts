import { Controller, Get, Res, Param, HttpStatus, Put, Header, Body, Patch, Delete, UseFilters, UseGuards, Query } from '@nestjs/common';
import { AdminWorldService } from './admin-world.service';
import { MyLogger } from 'src/logger/logger';
import World from '../world/world.entity';
import { Response } from 'express';
import { WorldUpdateDto } from '../world/dto/world.update.dto';
import { WorldPatchDto } from '../world/dto/world.patch.dto';
import { Roles } from 'src/auth/guard/roles.decorator';
import { ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from 'src/exception/exception.filter';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/guard/roles.guard';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { SelectQueryBuilder } from 'typeorm';

@ApiTags('admin-worlds')
@UseFilters(HttpExceptionFilter)
@Controller('admin-world')
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class AdminWorldController {
    constructor(private adminWorldService: AdminWorldService, private LOGGER: MyLogger) {
        this.LOGGER.setContext("AdminWorldController");
    }

    @Roles('admin')
    @Get(':id')
    async findOne(@Res() res: Response, @Param() params) {
        const world: World = await this.adminWorldService.find(params.id);
        res.status(HttpStatus.OK).json(world);
    }

    @Roles('admin')
    @Get('')
    async findAll(@Res() res: Response, @Query("page") page = 1,
        @Query("limit") limit = 10, @Query("userId") userId: string) {
        this.LOGGER.debug("findAll");
        limit = limit > 100 ? 100 : limit;
        res.status(HttpStatus.OK).json(await this.adminWorldService.findAll(userId, { page, limit, route: "http://localhost:3000/world/" }));
    }

    @Roles('admin')
    @Put(':id')
    @Header('Cache-Control', 'none')
    async update(@Res() res: Response, @Param('id') id: string, @Body() worldUpdateDto: WorldUpdateDto) {
        const world: World = await this.adminWorldService.update(id, worldUpdateDto);
        res.status(HttpStatus.OK).json(world);
    }

    @Roles('admin')
    @Patch(':id')
    @Header('Cache-Control', 'none')
    async patch(@Res() res: Response, @Param('id') id: string, @Body() worldPatchDto: WorldPatchDto) {
        const world: World = await this.adminWorldService.patch(id, worldPatchDto)
        res.status(HttpStatus.OK).json(world);
    }

    @Roles('admin')
    @Delete(':id')
    @Header('Cache-Control', 'none')
    async remove(@Res() res: Response, @Param('id') id: string) {
        await this.adminWorldService.delete(id);
        res.status(HttpStatus.NO_CONTENT).json("");
    }
}
