import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, DeleteDateColumn, UpdateDateColumn, CreateDateColumn } from "typeorm";
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
@Exclude()
export default class World extends BaseEntity {

    @ApiProperty({ description: "Unique Identifier to reference the world" })
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ApiProperty({ description: "Owner identifier" })
    @IsNotEmpty()
    @Column()
    userId: string;

    @ApiProperty({ description: "Creation date" })
    @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', precision: 3 })
    created: Date;

    @ApiProperty({ description: "Update date" })
    @UpdateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', precision: 3 })
    updated: Date;

    @ApiProperty({ description: "Delete date" })
    @DeleteDateColumn({ type: 'timestamp', precision: 3 })
    deleted: Date;

    @ApiProperty({ description: "Name of the world" })
    @Column()
    name: string;

}