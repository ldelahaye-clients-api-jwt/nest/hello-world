import { Injectable, BadRequestException } from '@nestjs/common';
import { paginate, Pagination, IPaginationOptions } from 'nestjs-typeorm-paginate';

import World from './world.entity';
import { WorldRepository } from './world.repository';
import { MyLogger } from 'src/logger/logger';
import { WorldCreateDto } from './dto/world.create.dto';
import { WorldUpdateDto } from './dto/world.update.dto';
import { WorldPatchDto } from './dto/world.patch.dto';
import { SelectQueryBuilder } from 'typeorm';

@Injectable()
export class WorldService {

    constructor(private worldRepository: WorldRepository, private LOGGER: MyLogger) {
        this.LOGGER.setContext("WorldService");
    }

    async findAll(userId: string, options: IPaginationOptions): Promise<Pagination<World>> {
        const queryBuilder: SelectQueryBuilder<World> = this.worldRepository.createQueryBuilder('w');
        queryBuilder.where('w.userId= :userId', { userId }).addOrderBy('w.updated', 'DESC');
        this.LOGGER.debug(JSON.stringify(options));
        return await paginate<World>(queryBuilder, options);
    }

    async create(worldCreateDto: WorldCreateDto, userId: string): Promise<World> {
        const world: World = World.create();
        const { name } = worldCreateDto;
        world.name = name;
        world.userId = userId;
        return await this.worldRepository.createWorld(world);
    }

    async find(id: string, userId: string): Promise<World> {
        this.LOGGER.debug('find', id);
        return await this.worldRepository.findOneWorld(id, userId);
    }

    async update(idWorld: string, worldUpdateDto: WorldUpdateDto, userId: string): Promise<World> {
        this.LOGGER.debug("update", idWorld);
        const world: World = World.create();
        const { id, name } = worldUpdateDto;
        if (id != idWorld) {
            throw new BadRequestException('ids.not.match')
        }
        world.id = id;
        world.name = name;
        return await this.worldRepository.updateWorld(idWorld, world, userId);
    }

    async patch(idWorld: string, worldPatchDto: WorldPatchDto, userId: string): Promise<World> {
        this.LOGGER.debug("update", idWorld);
        const { name } = worldPatchDto;

        const world: World = await this.worldRepository.findOneWorld(idWorld, userId);
        //Patch only name attribut
        world.name = name;

        return await this.worldRepository.updateWorld(idWorld, world, userId);
    }

    async delete(id: string, userId: string): Promise<void> {
        this.LOGGER.debug('delete', id);
        await this.worldRepository.removeWorld(id, userId);
    }
}
