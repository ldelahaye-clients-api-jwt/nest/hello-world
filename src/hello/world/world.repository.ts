import { EntityRepository, Repository } from "typeorm";
import World from "./world.entity";

@EntityRepository(World)
export class WorldRepository extends Repository<World> {

    createWorld = async (world: World) => {
        return await this.save(world);
    };

    findOneWorld = async (id: string, userId: string) => {
        return this.findOneOrFail({ where: { id, userId } });
    };

    updateWorld = async (id: string, world: World, userId: string) => {
        await this.findOneOrFail({ where: { id, userId } });
        return this.save(world);
    };

    removeWorld = async (id: string, userId: string) => {
        await this.findOneOrFail({ where: { id, userId } });
        return await this.softDelete(id);
    };
}