import { IsNotEmpty, IsUUID, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class WorldUpdateDto {

    @ApiProperty({ description: "Unique Identifier to reference the world" })
    @IsNotEmpty()
    @IsUUID("4")
    id: string;

    @ApiProperty({ description: "Name of the world", minLength: 10 })
    @IsNotEmpty()
    @MinLength(10)
    name: string;

}