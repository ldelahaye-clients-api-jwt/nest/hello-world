import { IsNotEmpty, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class WorldCreateDto {

    @ApiProperty({ description: "Name of the world", minLength: 10 })
    @IsNotEmpty()
    @MinLength(10)
    name: string;
}