import { MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class WorldPatchDto {

    @ApiProperty({ description: "Name of the world", minLength: 10, required: false })
    @MinLength(10)
    name: string;

}