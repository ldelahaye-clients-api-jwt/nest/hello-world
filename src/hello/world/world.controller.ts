import { Controller, Get, Post, Header, Param, Body, Put, Res, HttpStatus, Delete, UseGuards, UseFilters, Patch, Query } from '@nestjs/common';
import { Response } from 'express';

import { WorldService } from './world.service';
import World from './world.entity';
import { MyLogger } from 'src/logger/logger';
import { WorldCreateDto } from './dto/world.create.dto';
import { HttpExceptionFilter } from 'src/exception/exception.filter';
import { WorldUpdateDto } from './dto/world.update.dto';
import { User } from 'src/auth/user.decorator';
import { WorldPatchDto } from './dto/world.patch.dto';
import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
@ApiTags('worlds')
@UseFilters(HttpExceptionFilter)
@UseGuards(AuthGuard('jwt'))
@Controller('world')
export class WorldController {

    constructor(private worldService: WorldService, private LOGGER: MyLogger) {
        this.LOGGER.setContext("WorldController");
    }

    @Post()
    @Header('Cache-Control', 'none')
    async create(@Res() res: Response, @Body() worldCreateDto: WorldCreateDto, @User('userId') userId: string) {
        this.LOGGER.debug("create", JSON.stringify(worldCreateDto));
        const world: World = await this.worldService.create(worldCreateDto, userId);
        res.status(HttpStatus.CREATED).json(world);
    }

    @Get('')
    async findAll(@Res() res: Response, @Query("page") page = 1,
        @Query("limit") limit = 10, @User('userId') userId: string) {
        this.LOGGER.debug("findOne");
        limit = limit > 100 ? 100 : limit;
        res.status(HttpStatus.OK).json(await this.worldService.findAll(userId, { page, limit, route: "http://localhost:3000/world/" }));

    }

    @Get(':id')
    async findOne(@Res() res: Response, @Param() params, @User('userId') userId: string) {
        this.LOGGER.debug("findOne");
        const world: World = await this.worldService.find(params.id, userId);
        res.status(HttpStatus.OK).json(world);
    }

    @Put(':id')
    @Header('Cache-Control', 'none')
    async update(@Res() res: Response, @Param('id') id: string, @Body() worldUpdateDto: WorldUpdateDto, @User('userId') userId: string) {
        this.LOGGER.debug(JSON.stringify(worldUpdateDto));
        const world: World = await this.worldService.update(id, worldUpdateDto, userId)
        res.status(HttpStatus.OK).json(world);
    }

    @Patch(':id')
    @Header('Cache-Control', 'none')
    async patch(@Res() res: Response, @Param('id') id: string, @Body() worldPatchDto: WorldPatchDto, @User('userId') userId: string) {
        this.LOGGER.debug(JSON.stringify(worldPatchDto));
        const world: World = await this.worldService.patch(id, worldPatchDto, userId)
        res.status(HttpStatus.OK).json(world);
    }

    @Delete(':id')
    @Header('Cache-Control', 'none')
    async remove(@Res() res: Response, @Param('id') id: string, @User('userId') userId: string) {
        await this.worldService.delete(id, userId);
        res.status(HttpStatus.NO_CONTENT).json("");
    }

}
