import { Module, NestModule, MiddlewareConsumer, Logger } from '@nestjs/common';
import { WorldController } from './world/world.controller';
import { WorldService } from './world/world.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WorldRepository } from './world/world.repository';
import { LoggerModule } from 'src/logger/logger.module';
import { LoggerAccess } from 'src/logger/logger.access';
import { AdminWorldController } from './admin-world/admin-world.controller';
import { AdminWorldService } from './admin-world/admin-world.service';

@Module({
    imports: [TypeOrmModule.forFeature([WorldRepository]), LoggerModule],
    exports: [TypeOrmModule],
    providers: [WorldService, AdminWorldService],
    controllers: [WorldController, AdminWorldController]
})
export class WorldModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LoggerAccess)
    }
}