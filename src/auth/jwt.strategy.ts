import * as fs from 'fs';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { JwtProperty } from 'src/property/jwt.property';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(readonly jwtProperty: JwtProperty) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: fs.readFileSync(jwtProperty.publicKeyFile, 'utf8'),
            algorithms: jwtProperty.algorithms,
            issuer: jwtProperty.issuer,
            audience: jwtProperty.audience,
        });
    }

    async validate(payload: any) {
        return { userId: payload.sub, username: payload.username, roles: payload.roles };
    }
}