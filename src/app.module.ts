import { Module } from '@nestjs/common';
import { WorldModule } from './hello/world.module';
import { PropertyModule } from './property/property.module';
import { DatabaseModule } from './database/database.module';
import { AuthModule } from './auth/auth.module';
import { LoggerModule } from './logger/logger.module';

@Module({
    imports: [
        PropertyModule,
        DatabaseModule,
        WorldModule,
        AuthModule,
        LoggerModule,
    ]
})
export class AppModule { }
