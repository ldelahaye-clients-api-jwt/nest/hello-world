import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Request, Response } from 'express';
import { MyLogger } from 'src/logger/logger';

@Catch()
@Injectable()
export class HttpExceptionFilter implements ExceptionFilter {

    constructor(private LOGGER: MyLogger) {
        LOGGER.setContext("MA-Exception");
    }

    catch(exception: unknown, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        let statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
        let name = "INTERNAL_SERVER_ERROR";
        let message = "Internal server error";
        let stack = "";
        let detail = "";
        // Manage HTTP Exception
        if (exception instanceof HttpException) {
            const httpException: HttpException = exception;
            statusCode = httpException.getStatus();
            name = httpException.name;
            message = httpException.message;
            // Check if NODE_ENV
            stack = httpException.stack;
            if (process.env.NODE_ENV !== "prod") {
                detail = stack;
            }

        } else if (exception instanceof Error) {
            // Manage Internal Server Error
            this.LOGGER.error(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            const error: Error = exception;
            name = error.name;
            message = error.message;
            stack = error.stack;
            if (process.env.NODE_ENV !== "prod") {
                detail = stack;
            }
        }

        if (statusCode >= HttpStatus.INTERNAL_SERVER_ERROR) {
            this.LOGGER.error(name, message, stack);
        } else {
            this.LOGGER.warn(name, message, stack);
        }

        response
            .status(statusCode)
            .json({
                path: request.url,
                id: this.LOGGER.getTID(),
                statusCode,
                message,
                detail,
                timestamp: new Date().toISOString(),
            });
    }
}