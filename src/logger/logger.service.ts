import { Injectable, Scope, Logger } from '@nestjs/common';
import { v4 as uuid } from 'uuid';

/**
 * Enum to keep track of the different levels available.
 */
enum Level {
    VERBOSE = "verbose",
    DEBUG = "debug",
    INFO = "info",
    WARN = "warn",
    ERROR = "error"
}

@Injectable({ scope: Scope.REQUEST })
export class LoggerService {
    private TID: string;

    constructor(private LOGGER: Logger) {
        this.TID = uuid();
    }

    info(context: string, message: string, args: string[]) {
        this.formatMessage(context, Level.INFO, message, args, null);
    }

    warn(context: string, message: string, args: string[]) {
        this.formatMessage(context, Level.WARN, message, args, null);
    }

    debug(context: string, message: string, args: string[]) {
        this.formatMessage(context, Level.DEBUG, message, args, null);
    }

    error(context: string, message: string, trace?: string, args?: string[]) {
        this.formatMessage(context, Level.ERROR, message, args, null);
    }

    verbose(context: string, message: string, args: string[]) {
        this.formatMessage(context, Level.VERBOSE, message, args, null);
    }

    private formatMessage(context: string, level: Level, message: string, args: string[], trace: string) {
        const formatMessage = `[${this.TID}] ${message} ${args ? args : ""}`;
        this.writeLog(context, level, formatMessage, trace);
    }

    private writeLog(context: string, level: Level, message: string, trace?: string) {
        switch (level) {
            case Level.DEBUG: this.LOGGER.debug(message, context); break;
            case Level.ERROR: this.LOGGER.error(message, trace, context); break;
            case Level.INFO: this.LOGGER.log(message, context); break;
            case Level.VERBOSE: this.LOGGER.verbose(message, context); break;
            case Level.WARN: this.LOGGER.warn(message, context); break;
        }
    }

    getTID(): string {
        return this.TID;
    }
}
