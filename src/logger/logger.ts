import { Scope, Injectable } from "@nestjs/common";
import { LoggerService } from "./logger.service";
import { Logger, QueryRunner } from "typeorm";

@Injectable({ scope: Scope.TRANSIENT })
export class MyLogger implements Logger {
    private context: string;

    constructor(private loggerService: LoggerService) {
    }

    info(message: string, ...args) {
        this.loggerService.info(this.context, message, args);
    }

    warn(message: string, ...args: string[]) {
        this.loggerService.warn(this.context, message, args);
    }

    debug(message: string, ...args: string[]) {
        this.loggerService.debug(this.context, message, args);
    }

    error(message: string, trace?: string, ...args: string[]) {
        this.loggerService.error(this.context, message, trace, args);
    }

    verbose(message: string, ...args: string[]) {
        this.loggerService.verbose(this.context, message, args);
    }

    setContext(context: string) {
        this.context = context;
    }

    public getTID(): string {
        return this.loggerService.getTID();
    }

    public logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner) {
        this.loggerService.verbose(this.context, query, parameters);
    }

    public logQueryError(error: string, query: string, parameters?: any[], queryRunner?: QueryRunner) {
        this.loggerService.error(this.context, query, error, parameters);
    }

    public logQuerySlow(time: number, query: string, parameters?: any[], queryRunner?: QueryRunner) {
        this.loggerService.warn(this.context, `Slow query took ${time} ms: %o %o %o`, [query]);
    }

    public logSchemaBuild(message: string, queryRunner?: QueryRunner) {
        this.loggerService.info(this.context, message, null);
    }

    public logMigration(message: string, queryRunner?: QueryRunner) {
        this.loggerService.info(this.context, message, null);
    }

    public log(level: 'log' | 'info' | 'warn', message: any, queryRunner?: QueryRunner) {
        switch (level) {
            case 'log': this.loggerService.info(this.context, message, null); break;
            case 'info': this.loggerService.info(this.context, message, null); break;
            case 'warn': this.loggerService.warn(this.context, message, null); break;
        }
    }
}