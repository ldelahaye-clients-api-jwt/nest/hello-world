import { Module, Logger } from '@nestjs/common';
import { LoggerAccess } from 'src/logger/logger.access';
import { LoggerService } from './logger.service';
import { MyLogger } from './logger';

@Module({
    imports: [Logger],
    providers: [LoggerAccess, LoggerService, MyLogger, Logger],
    exports: [LoggerAccess, LoggerService, MyLogger]
})
export class LoggerModule {
}