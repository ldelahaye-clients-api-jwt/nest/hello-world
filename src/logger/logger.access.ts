import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { MyLogger } from './logger';

@Injectable()
export class LoggerAccess implements NestMiddleware {

    constructor(private LOGGER: MyLogger) {
        LOGGER.setContext('LoggerAccess');
    }

    use(req: Request, res: Response, next: Function) {
        this.LOGGER.info("START", req.url);
        next();
        this.LOGGER.info("END", res.statusCode.toString());
    }
}
