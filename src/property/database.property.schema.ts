import * as Joi from 'joi';

export enum DATABASE_TYPES {
    MARIADB_DATABASE_TYPE = 'mariadb',
}

export const DATABASE_PROPERTY_SCHEMA: Joi.SchemaMap = {
    DATABASE_TYPE: Joi.string().valid(Object.values(DATABASE_TYPES)).required(),
    DATABASE_HOST: Joi.string().required(),
    DATABASE_PORT: Joi.number().required(),
    DATABASE_USERNAME: Joi.string().required(),
    DATABASE_PASSWORD: Joi.string().required(),
    DATABASE_NAME: Joi.string().required(),
    DATABASE_SYNCHRONIZE: Joi.boolean().default(false),
    DATABASE_DROP_SCHEMA: Joi.boolean().default(false),
    DATABASE_LOGGING: Joi.boolean().default(false),
};
