import { Injectable } from '@nestjs/common';
import { PropertyService } from './property.service';

@Injectable()
export class JwtProperty {
    constructor(
        private readonly propertyService: PropertyService,
    ) { }

    get publicKeyFile(): string {
        return this.propertyService.properties.JWT_PUBLIC_KEY_FILE;
    }

    get algorithms(): string[] {
        return Array.from(this.propertyService.properties.JWT_ALGORITHMS);
    }

    get issuer(): string {
        return this.propertyService.properties.JWT_ISSUER;
    }

    get audience(): string {
        return this.propertyService.properties.JWT_AUDIENCE;
    }
}
