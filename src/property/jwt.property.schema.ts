import * as Joi from 'joi';

export const JWT_PROPERTY_SCHEMA: Joi.SchemaMap = {
    JWT_PUBLIC_KEY_FILE: Joi.string().required(),
    JWT_ALGORITHMS: Joi.array().items(Joi.string()).required(),
    JWT_ISSUER: Joi.string().required(),
    JWT_AUDIENCE: Joi.string().allow('').default(''),
};
