import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Joi from 'joi';
import { DATABASE_PROPERTY_SCHEMA } from './database.property.schema';
import { JWT_PROPERTY_SCHEMA } from './jwt.property.schema';
// import { JWT_PROPERTY_SCHEMA } from './jwt.property.schema';
// import { HEALTH_PROPERTY_SCHEMA } from './health.property.schema';
// import { LOGGER_PROPERTY_SCHEMA } from './logger.property.schema';
// import { SWAGGER_PROPERTY_SCHEMA } from './swagger.property.schema';
// import { REQUEST_PROPERTY_SCHEMA } from './request.property.schema';
// import { SECURITY_PROPERTY_SCHEMA } from './security.property.schema';
// import { PAGINATION_PROPERTY_SCHEMA } from './pagination.property.schema';
// import { SANITIZER_PROPERTY_SCHEMA } from './sanitizer.property.schema';

export interface PropertyUsers {
    [username: string]: string;
}

export class PropertyService {
    private readonly validatedProperties: { [key: string]: string };

    constructor(filePath: string) {
        console.log("Property filepath", filePath);
        const properties: { [key: string]: string } = dotenv.parse(fs.readFileSync(filePath));
        const globalPropertySchema: Joi.SchemaMap = {};
        Object.assign(
            globalPropertySchema,
            DATABASE_PROPERTY_SCHEMA,
            JWT_PROPERTY_SCHEMA,
            // HEALTH_PROPERTY_SCHEMA,
            // LOGGER_PROPERTY_SCHEMA,
            // SWAGGER_PROPERTY_SCHEMA,
            // REQUEST_PROPERTY_SCHEMA,
            // SECURITY_PROPERTY_SCHEMA,
        );
        const { error, value: validatedProperties } = Joi.validate(
            properties,
            Joi.object(globalPropertySchema),
        );
        if (error) {
            throw new Error(`Config validation error: ${error.message}`);
        }
        this.validatedProperties = validatedProperties;
    }

    get properties(): { [key: string]: string } {
        return this.validatedProperties;
    }
}
