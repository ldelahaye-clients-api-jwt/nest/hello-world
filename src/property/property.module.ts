import { Module, Global } from '@nestjs/common';
import { DatabaseProperty } from './database.property';
import { PropertyService } from './property.service';
import { JwtProperty } from './jwt.property';
// import { HealthProperty } from './health.property';
// import { LoggerProperty } from './logger.property';
// import { SwaggerProperty } from './swagger.property';
// import { RequestProperty } from './request.property';
// import { SecurityProperty } from './security.property';


@Global()
@Module({
    providers: [
        {
            provide: PropertyService,
            useValue: new PropertyService(`${process.env.APPLICATION_MAHELLO_ENV}`),
        },
        DatabaseProperty,
        JwtProperty,
        // HealthProperty,
        // LoggerProperty,
        // SwaggerProperty,
        // RequestProperty,
        // SecurityProperty,
    ],
    exports: [
        DatabaseProperty,
        JwtProperty,
        // HealthProperty,
        // LoggerProperty,
        // SwaggerProperty,
        // RequestProperty,
        // SecurityProperty,
    ],
})
export class PropertyModule {
}
