## Description

[Micro API Hello - MA-Hello](https://gitlab.com/ldelahaye-clients-api-jwt/nest/template) is a starter kit for middleware or backend API in Micro Application Architecture.

---

## Try it with docker

### Clone with SSH
git clone git@gitlab.com:ldelahaye-clients-api-jwt/nest/hello-world.git

### Clone with HTTPS
git clone https://gitlab.com/ldelahaye-clients-api-jwt/nest/hello-world.git

### Start it
docker-compose up

### Play with Swagger
Click on : http://localhost:3000/api-docs/#/

## Installation

### Use an external configuration file from the environment variable `APPLICATION_MAHELLO_ENV`

- Copy the files `mahello.dev.env.dist` in your local properties folder :

```bash
#RECOMMENDED PATH :
# /etc/ma/mahello.dev.env :
cp mahello.dev.env.dist /etc/ma/mahello.dev.env
```

- Create the `APPLICATION_MAHELLO_ENV` environment variable with the value of the previous path you choose. This variable must be in your `/etc/environment` file. Restart your user's session or your PC

```
APPLICATION_MAHELLO_ENV="/etc/ma/mahello.dev.env"
```


### Create database
```sql
CREATE DATABASE ma_hello DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
CREATE USER 'ma_hello'@'localhost' IDENTIFIED BY 'putapasswordhere';
GRANT ALL ON ma_hello.* TO 'ma_hello'@'localhost';
```

  - Change the values that need to be changed in the `/etc/ma/mahello.dev.env` file :
     * DATABASE_NAME
     * DATABASE_USERNAME
     * DATABASE_PASSWORD
     ...


```bash
$ npm install
```

---

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

---

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

---

## Documentation

### SWAGGER DOCS

[LOCAL API DOCS](http://localhost:3000/api-docs/#/)

### Micro Application Architecture 

![Micro Application Architecture](docs/diagrams/images/micro.application.architecture.png)


### Security

JWT Secure API [RFC 7519](https://tools.ietf.org/html/rfc7519)

Asymmetric key signatures for JWT Authorization. Algorithm define : "JWT_ALGORITHMS"

Define the path of the public key of the IDP certificate : "JWT_PUBLIC_KEY_FILE"

Example JWT for default user:
```

{
  "sub": "1234567890",        # User ID
  "name": "John Doe",         # username
  "iss": "micro-application", # Name of issuer 
  "aud": "ma-hello",          # audience
  "iat": 1516239022           # creation date
}
```

Example JWT for admin user:
```

{
  "sub": "1234567890",        # User ID
  "name": "John Doe",         # username
  "iss": "micro-application", # Name of issuer
  "aud": "ma-hello",          # audience
  "iat": 1516239022,          # creation date
  "roles": [                  # roles
    "admin"
  ]
}
```

### Roles

Access to web service /world for all authenticated user
Access to web service /admin-world for authenticated user with role "admin"

### Access to Micro Application Hello with JWT

For developpement environment, you can use [jwt.private.key](jwt.private.key) file to [generate new token](https://jwt.io/)  with algorithm PS256

![Access to Micro API Hello](docs/diagrams/images/auth.success.png)

### [Sequence diagram refresh JWT](docs/diagrams/images/jwt.refresh.png)

### [Sequence diagram unauthorized Access](docs/diagrams/images/jwt.forbidden.png)

---

## Stay in touch

- Author - [Ludovic DELAHAYE](https://www.linkedin.com/in/ludovic-delahaye-110a7094/)

---

## License

  Micro Application Template is [MIT licensed](LICENSE).
